# FastAPI

This presentation is a tour of what FastAPI can do.

## Why FastAPI?

(start with why)

[FastAPI](https://fastapi.tiangolo.com/)
makes it easy to make quickly make
[web API](https://en.wikipedia.org/wiki/Web_API)s.

[API](https://en.wikipedia.org/wiki/API)s are how programs talk to each other.

[JSON](https://wikipedia.org/wiki/JSON) is FastAPI's output format.

## Good

- good documentation
- good tutorial
- easy input validation (this is a superpower)
- seems pretty sane
- leads to rather DRY code
- automatically generated documentation (another superpower)
- support async (which is optional)
- many contributors

## Smells

- Can not have an Enum of ints.
- Its openapi.json output for dictionaries (JSON objects)
  and lists (JSON arrays) seems broken.
- Main developer completely dominates.
```
git log --pretty=format:'%ae %an' --all | sort | uniq -c | sort -n | awk '{sum += $1; print sum, $0}'
```

## Follow Along

Follow along during the presentation with the following.

View this file at https://gitlab.com/james-prior/fastapi-presentation/-/blob/master/README.md.

### Installation

```
git clone https://gitlab.com/james-prior/fastapi-presentation.git
cd fastapi-presentation
pipenv install --dev
```
### Start API Server
```
pipenv run uvicorn main:app --reload
```

## Automatically Generated Documentation

It generates documentation for the API in several formats,
incuding [OpenAPI](https://en.wikipedia.org/wiki/OpenAPI_Specification).

- http://127.0.0.1:8000/openapi.json
- http://127.0.0.1:8000/docs
- http://127.0.0.1:8000/redoc

```
curl -s -X GET http://127.0.0.1:8000/openapi.json -H 'accept: application/json'
curl -s -X GET http://127.0.0.1:8000/openapi.json -H 'accept: application/json' | jq --indent 4 -C | less -R
```

## hello world

`git checkout 1`

excerpt of code from `main.py`:
```
@app.get("/")
def read_root():
    return {"Hello": "World"}
```
note:
- `.get` method of decorator specifies HTTP GET method.
- `"/"` specifies path in URL.
- Return value is dictionary which will be converted to JSON.
- Function name will appear in documentation, but otherwise does not matter.
- FastAPI does much with little code.

http://127.0.0.1:8000/
- show in browser
- show on command line
```
curl -s -X GET http://127.0.0.1:8000/ -H 'accept: application/json'
curl -v -X GET http://127.0.0.1:8000/ -H 'accept: application/json'
curl -s -X GET http://127.0.0.1:8000/ -H 'accept: application/json' | jq
curl -s -X GET http://127.0.0.1:8000/ -H 'accept: application/json' | jq -C
curl -s -X GET http://127.0.0.1:8000/ -H 'accept: application/json' | jq -M
curl -s -X GET http://127.0.0.1:8000/ -H 'accept: application/json' | jq --indent 4
```

## input validation

Input is validated. Output is _not_ validated.

simple example of integer

`git checkout 1.1`

excerpt of code from `main.py`:
```
@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}
```
note:
- path parameter
- query parameter
- annotations
- function name will appear in documentation, but otherwise does not matter

http://127.0.0.1:8000/items/17?q=quick%20brown%20fox
- show in browser
- show in command line

- show validation reject a query
http://127.0.0.1:8000/items/one?q=quick%20brown%20fox

## async

async is supported

`git checkout 2`

excerpts of code from `main.py`:
```
@app.get("/sync-sleep/{duration}")
def sleep(duration: float):
    time.sleep(duration)
    return f'sync slept for {duration} seconds'
```
```
@app.get("/async-sleep/{duration}")
async def sleep(duration: float):
    await asyncio.sleep(duration)
    return f'async slept for {duration} seconds'
```

note:
- differences between synchronous and asynchronous code
- Both functions have same name yet the latter one does not overwrite the earlier one.

### demonstrate
```
alias s='curl -s -X GET http://127.0.0.1:8000/sync-sleep/3 -H "accept: application/json" | jq --indent 4'
s & s &
```
```
alias s='curl -s -X GET http://127.0.0.1:8000/async-sleep/3 -H "accept: application/json" | jq --indent 4'
s & s &
```

## sending data in body

`git checkout 3`

show excerpts of code from `main.py`:
```
class Name(BaseModel):
    first: constr(min_length=2, max_length=8, regex=r'^[A-Z][A-Za-z]*$')
    last: constr(min_length=2, max_length=8, regex=r'^[A-Z][A-Za-z]*$')
```
```
class Adult(BaseModel):
    name: Name
    age: confloat(ge=18)
```
```
@app.post("/adult")
async def save_adult(adult: Adult):
    return adult
```

note:
- models can be nested
- powerful validations performed by
  [`constr`](https://pydantic-docs.helpmanual.io/usage/types/#arguments-to-constr) and
  [`confloat`](https://pydantic-docs.helpmanual.io/usage/types/#arguments-to-confloat) from
  [`pydantic`](https://pypi.org/project/pydantic/)

### demonstrate
happy case
```
curl -s -X POST http://127.0.0.1:8000/adult -H "accept: application/json" -H 'Content-Type: application/json' -d '{ "name":{"first":"John","last":"Doe"},"age":21}' | jq --indent 4
```
rejections
```
curl -s -X POST http://127.0.0.1:8000/adult -H "accept: application/json" -H 'Content-Type: application/json' -d '{ "name":{"first":"John","last":"doe"},"age":21}' | jq --indent 4
curl -s -X POST http://127.0.0.1:8000/adult -H "accept: application/json" -H 'Content-Type: application/json' -d '{ "name":{"first":"John","last":"Doppelganger"},"age":21}' | jq --indent 4
curl -s -X POST http://127.0.0.1:8000/adult -H "accept: application/json" -H 'Content-Type: application/json' -d '{ "name":{"first":"John","last":"Doe"},"age":17.5}' | jq --indent 4
```

## show mix of operations

RPN calculator example

`git checkout 3`

### demonstrate

```
curl -s -X GET http://127.0.0.1:8000/stack -H "accept: application/json" | jq --indent 4
curl -s -X DELETE http://127.0.0.1:8000/stack -H "accept: application/json" | jq --indent 4

curl -s -X POST http://127.0.0.1:8000/push/3.14159 -H "accept: application/json" | jq --indent 4
curl -s -X POST http://127.0.0.1:8000/push/2.71828 -H "accept: application/json" | jq --indent 4
curl -s -X POST http://127.0.0.1:8000/pop -H "accept: application/json" | jq --indent 4
curl -v -X POST http://127.0.0.1:8000/pop -H "accept: application/json" | jq --indent 4

curl -s -X POST http://127.0.0.1:8000/add -H "accept: application/json" | jq --indent 4
curl -s -X POST http://127.0.0.1:8000/subtract -H "accept: application/json" | jq --indent 4
curl -s -X POST http://127.0.0.1:8000/multiply -H "accept: application/json" | jq --indent 4
curl -s -X POST http://127.0.0.1:8000/divide -H "accept: application/json" | jq --indent 4
```

### note
- Same path can be used by different methods.
  For example, `/stack` path can be used by both `GET` and `DELETE` HTTP methods.
- Provoke errors to show custom responses.
- FastAPI provides nice meaningful names for HTTP status codes
  such as `status.HTTP_403_FORBIDDEN` instead of meaningless `403`
  (We should use such in our production code
  instead of those raw meaningless numbers.)

## testing

FastAPI makes tests easy.

```
git checkout 4
```

```
def test_clear_stack():
    response = client.delete("/stack")

    assert response.status_code == 200
    assert response.json() == []
    assert stack == []
```

### demonstrate
```
pipenv run pytest --color=yes -f .
```

### note 
- the test makes a web request (does not just call function)
- tests are easy to write

## links

- [FastAPI website](https://fastapi.tiangolo.com/)
- [FastAPI git repository](https://github.com/tiangolo/fastapi)
- [FastAPI tutorial](https://fastapi-tutorial.readthedocs.io/en/latest/)
- [git repository for this presentation](https://gitlab.com/james-prior/fastapi-presentation)
- [HTTP request methods](https://en.wikipedia.org/wiki/HTTP#Request_methods)
- [Wikipedia article about APIs](https://en.wikipedia.org/wiki/API)
- [Wikipedia article about web APIs](https://en.wikipedia.org/wiki/Web_API)
- [Pydantic](https://pypi.org/project/pydantic/)

## todo

Use custom Stack model for openapi.json output.
Maybe describe all output with models.
Polish dox.
Rework from beginning.
Add 403 response to bureaucracy for apply_math_operation.
Annotate tags.
