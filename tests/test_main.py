from fastapi.testclient import TestClient

from main import app, stack

client = TestClient(app)


def test_clear_stack():
    response = client.delete("/stack")

    assert response.status_code == 200
    assert response.json() == []
    assert stack == []
