import asyncio
import time
from enum import Enum
from operator import add, sub, mul, truediv

from fastapi import FastAPI, Body, Response, status
from pydantic import BaseModel, constr, confloat


class ModelName(str, Enum):
    alexnet = 'alexnet'
    resnet = 'resnet'
    lenet = 'lenet'


app = FastAPI()


@app.get("/models/{model_name}")
async def get_model(model_name: ModelName):
    if model_name == ModelName.alexnet:
        return {"model_name": model_name, "message": "Deep Learning FTW!"}

    if model_name.value == ModelName.lenet:
        return {"model_name": model_name, "message": "LeCNN all the images"}

    return {"model_name": model_name, "message": "Have some residuals"}


@app.get("/sync-sleep/{duration}")
def sleep(duration: float):
    time.sleep(duration)
    return f'sync slept for {duration} seconds'


@app.get("/async-sleep/{duration}")
async def sleep(duration: float):
    await asyncio.sleep(duration)
    return f'async slept for {duration} seconds'


class Name(BaseModel):
    first: constr(min_length=2, max_length=8, regex=r'^[A-Z][A-Za-z]*$')
    last: constr(min_length=2, max_length=8, regex=r'^[A-Z][A-Za-z]*$')



class Adult(BaseModel):
    name: Name
    age: confloat(ge=18)


@app.post("/adult")
async def save_adult(adult: Adult):
    return adult


stack = []


class Operation(str, Enum):
    add = 'add'
    subtract = 'subtract'
    multiply = 'multiply'
    divide = 'divide'


@app.get("/stack")
async def get_stack():
    return stack


@app.delete("/stack")
async def clear_stack():
    """Clears stack."""
    global stack

    stack = []
    return stack


@app.post("/push/{x}")
async def push_stack(x: float):
    """Pushes float provided in path parameter."""
    stack.append(x)
    return stack


@app.post("/pop")
async def pop_stack(response: Response):
    try:
        x = stack.pop()
    except IndexError:
        response.status_code = status.HTTP_412_PRECONDITION_FAILED
        return {'error message': 'stack empty'}
    return x, stack


@app.post("/{operation}")
async def apply_math_operation(operation: Operation, response: Response):
    if len(stack) < 2:
        response.status_code = status.HTTP_412_PRECONDITION_FAILED
        return {
            'error message':
            f'not enough data on stack to perform {operation} operation'
        }
    y = stack.pop()
    x = stack.pop()
    operation_functions = {
        'add': add,
        'subtract': sub,
        'multiply': mul,
        'divide': truediv,
    }
    operation_function = operation_functions[operation]
    try:
        z = operation_function(x, y)
    except ZeroDivisionError:
        stack.append(x)
        stack.append(y)

        response.status_code = status.HTTP_403_FORBIDDEN
        return {
            'error message': f'tried to divide {x} by zero'
        }
    stack.append(z)

    return stack
